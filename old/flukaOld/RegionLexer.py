# Generated from RegionLexer.g4 by ANTLR 4.7
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2")
        buf.write(u"\16w\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write(u"\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t")
        buf.write(u"\r\4\16\t\16\3\2\3\2\3\2\3\2\3\3\5\3#\n\3\3\3\3\3\3\3")
        buf.write(u"\3\3\3\4\3\4\3\4\6\4,\n\4\r\4\16\4-\3\5\5\5\61\n\5\3")
        buf.write(u"\5\6\5\64\n\5\r\5\16\5\65\3\6\3\6\5\6:\n\6\5\6<\n\6\3")
        buf.write(u"\6\6\6?\n\6\r\6\16\6@\3\6\5\6D\n\6\3\6\7\6G\n\6\f\6\16")
        buf.write(u"\6J\13\6\3\6\5\6M\n\6\3\6\5\6P\n\6\3\6\7\6S\n\6\f\6\16")
        buf.write(u"\6V\13\6\3\6\3\6\6\6Z\n\6\r\6\16\6[\5\6^\n\6\3\7\3\7")
        buf.write(u"\3\b\3\b\3\b\7\be\n\b\f\b\16\bh\13\b\3\t\3\t\3\t\3\t")
        buf.write(u"\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16\2\2\17\3")
        buf.write(u"\3\5\4\7\5\t\6\13\7\r\2\17\b\21\t\23\n\25\13\27\f\31")
        buf.write(u"\r\33\16\3\2\t\4\2\13\13\"\"\4\2C\\c|\6\2\62;C\\aac|")
        buf.write(u"\4\2--//\3\2\62;\7\2//\62;C\\aac|\5\2..\61\61<=\2\u0084")
        buf.write(u"\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13")
        buf.write(u"\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25")
        buf.write(u"\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\3\35")
        buf.write(u"\3\2\2\2\5\"\3\2\2\2\7(\3\2\2\2\t\60\3\2\2\2\13;\3\2")
        buf.write(u"\2\2\r_\3\2\2\2\17a\3\2\2\2\21i\3\2\2\2\23m\3\2\2\2\25")
        buf.write(u"o\3\2\2\2\27q\3\2\2\2\31s\3\2\2\2\33u\3\2\2\2\35\36\t")
        buf.write(u"\2\2\2\36\37\3\2\2\2\37 \b\2\2\2 \4\3\2\2\2!#\7\17\2")
        buf.write(u"\2\"!\3\2\2\2\"#\3\2\2\2#$\3\2\2\2$%\7\f\2\2%&\3\2\2")
        buf.write(u"\2&\'\b\3\3\2\'\6\3\2\2\2()\t\3\2\2)+\6\4\2\2*,\t\4\2")
        buf.write(u"\2+*\3\2\2\2,-\3\2\2\2-+\3\2\2\2-.\3\2\2\2.\b\3\2\2\2")
        buf.write(u"/\61\7/\2\2\60/\3\2\2\2\60\61\3\2\2\2\61\63\3\2\2\2\62")
        buf.write(u"\64\5\r\7\2\63\62\3\2\2\2\64\65\3\2\2\2\65\63\3\2\2\2")
        buf.write(u"\65\66\3\2\2\2\66\n\3\2\2\2\67<\7-\2\28:\7/\2\298\3\2")
        buf.write(u"\2\29:\3\2\2\2:<\3\2\2\2;\67\3\2\2\2;9\3\2\2\2<]\3\2")
        buf.write(u"\2\2=?\5\r\7\2>=\3\2\2\2?@\3\2\2\2@>\3\2\2\2@A\3\2\2")
        buf.write(u"\2AC\3\2\2\2BD\7\60\2\2CB\3\2\2\2CD\3\2\2\2DH\3\2\2\2")
        buf.write(u"EG\5\r\7\2FE\3\2\2\2GJ\3\2\2\2HF\3\2\2\2HI\3\2\2\2IL")
        buf.write(u"\3\2\2\2JH\3\2\2\2KM\7G\2\2LK\3\2\2\2LM\3\2\2\2MO\3\2")
        buf.write(u"\2\2NP\t\5\2\2ON\3\2\2\2OP\3\2\2\2PT\3\2\2\2QS\5\r\7")
        buf.write(u"\2RQ\3\2\2\2SV\3\2\2\2TR\3\2\2\2TU\3\2\2\2U^\3\2\2\2")
        buf.write(u"VT\3\2\2\2WY\7\60\2\2XZ\5\r\7\2YX\3\2\2\2Z[\3\2\2\2[")
        buf.write(u"Y\3\2\2\2[\\\3\2\2\2\\^\3\2\2\2]>\3\2\2\2]W\3\2\2\2^")
        buf.write(u"\f\3\2\2\2_`\t\6\2\2`\16\3\2\2\2ab\t\3\2\2bf\6\b\3\2")
        buf.write(u"ce\t\7\2\2dc\3\2\2\2eh\3\2\2\2fd\3\2\2\2fg\3\2\2\2g\20")
        buf.write(u"\3\2\2\2hf\3\2\2\2ij\t\b\2\2jk\3\2\2\2kl\b\t\2\2l\22")
        buf.write(u"\3\2\2\2mn\7-\2\2n\24\3\2\2\2op\7/\2\2p\26\3\2\2\2qr")
        buf.write(u"\7~\2\2r\30\3\2\2\2st\7*\2\2t\32\3\2\2\2uv\7+\2\2v\34")
        buf.write(u"\3\2\2\2\22\2\"-\60\659;@CHLOT[]f\4\b\2\2\2\3\2")
        return buf.getvalue()


class RegionLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    Whitespace = 1
    Newline = 2
    RegionName = 3
    Integer = 4
    Float = 5
    ID = 6
    Delim = 7
    Plus = 8
    Minus = 9
    Bar = 10
    LParen = 11
    RParen = 12

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ u"DEFAULT_MODE" ]

    literalNames = [ u"<INVALID>",
            u"'+'", u"'-'", u"'|'", u"'('", u"')'" ]

    symbolicNames = [ u"<INVALID>",
            u"Whitespace", u"Newline", u"RegionName", u"Integer", u"Float", 
            u"ID", u"Delim", u"Plus", u"Minus", u"Bar", u"LParen", u"RParen" ]

    ruleNames = [ u"Whitespace", u"Newline", u"RegionName", u"Integer", 
                  u"Float", u"Digit", u"ID", u"Delim", u"Plus", u"Minus", 
                  u"Bar", u"LParen", u"RParen" ]

    grammarFileName = u"RegionLexer.g4"

    def __init__(self, input=None, output=sys.stdout):
        super(RegionLexer, self).__init__(input, output=output)
        self.checkVersion("4.7")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    def sempred(self, localctx, ruleIndex, predIndex):
        if self._predicates is None:
            preds = dict()
            preds[2] = self.RegionName_sempred
            preds[6] = self.ID_sempred
            self._predicates = preds
        pred = self._predicates.get(ruleIndex, None)
        if pred is not None:
            return pred(localctx, predIndex)
        else:
            raise Exception("No registered predicate for:" + str(ruleIndex))

    def RegionName_sempred(self, localctx, predIndex):
            if predIndex == 0:
                return self.column == 1
         

    def ID_sempred(self, localctx, predIndex):
            if predIndex == 1:
                return self.column != 1
         


