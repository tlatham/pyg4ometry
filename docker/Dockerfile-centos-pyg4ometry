FROM centos:7

SHELL ["/bin/bash", "-c"]

WORKDIR /tmp

RUN yum -y install epel-release

# python
RUN yum -y install python3-pip python3 python3-devel python36-devel 

# X11 
RUN yum -y install xorg-x11-server-Xorg xorg-x11-xauth libX11-devel \
    libXpm-devel libXft-devel libXext-devel \
    libXt-devel

# OpenGL
RUN yum -y install mesa-libGL-devel

# mpfr, gmp
RUN yum -y install mpfr-devel gmp-devel

# GIT, wget, cmake3
RUN yum -y install git wget cmake3

# Modern compilers 
RUN yum -y install centos-release-scl && \
    yum -y install devtoolset-7

# CGAL
RUN wget https://github.com/CGAL/cgal/archive/refs/tags/v5.3.1.tar.gz && \
    tar zxf v5.3.1.tar.gz && \
    source scl_source enable devtoolset-7 && \
    mkdir build && \
    cd build && \
    cmake3 ../cgal-5.3.1 && \
    make && \
    make install && \
    cd ../ && \
    rm -rf build v5.3.1.tar.gz

# pyg4ometry deps 
RUN pip3 install cython pybind11 auditwheel
RUN yum -y install boost169-devel

# pyg4ometry
RUN git clone https://stewartboogert@bitbucket.org/jairhul/pyg4ometry.git 

RUN cd pyg4ometry && \
    source scl_source enable devtoolset-7 && \
    python3.6 setup.py build_ext